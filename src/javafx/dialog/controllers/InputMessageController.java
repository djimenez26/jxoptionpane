/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx.dialog.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author Certificaciones
 */
public class InputMessageController implements Initializable {

    private final String message;
    private final String imgPath;

    @FXML
    public Label lMessage;
    @FXML
    public ImageView imgMessage;
    @FXML
    public TextField tInputText;

    public InputMessageController(String message, String imgPath) {
        this.message = message;
        this.imgPath = imgPath;
    }

    @FXML
    public void closeMessage(ActionEvent e) {
        ((Node) e.getSource()).getScene().getWindow().hide();
    }

    @FXML
    public void closeMessageKey(KeyEvent e) {
        if (e.getCode() == KeyCode.ENTER) {
            ((Node) e.getSource()).getScene().getWindow().hide();
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.lMessage.setText(message);
        imgMessage.setImage(new Image(imgPath));
    }

}
