/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx.dialog.controllers;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 *
 * @author Certificaciones
 */
public class JXOptionPane extends Application {

    public static final int INFORMATION_MESSAGE = 1;
    public static final int WARNING_MESSAGE = 2;
    public static final int ERROR_MESSAGE = 3;
    public static final int YES_OPTION = 4;
    public static final int NO_OPTION = 5;

    private static String messageLogoPath;

    public static Integer showConfirmMessage(Window parent, String title, String message, Image logo) {
        try {
            FXMLLoader loader = new FXMLLoader(JXOptionPane.class.getResource("/javafx/dialog/templates/ConfirmMessage.fxml"));
            Stage messageStage = new Stage();

            messageStage.initModality(Modality.WINDOW_MODAL);
            if (logo != null) {
                messageStage.getIcons().add(logo);
            }
            messageStage.initOwner(parent);
            messageStage.setTitle(title);
            messageStage.setResizable(false);
            messageLogoPath = "javafx/dialog/img/information.png";
            ConfirmMessageController controller = new ConfirmMessageController(message, messageLogoPath);
            loader.setController(controller);
            Pane indexViewPane = loader.load();
            Scene indexViewScene = new Scene(indexViewPane);
            messageStage.setScene(indexViewScene);
            messageStage.showAndWait();
            return controller.returnOption;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static String showInputMessage(Window parent, String title, String message, Image logo) {
        try {
            FXMLLoader loader = new FXMLLoader(JXOptionPane.class.getResource("/javafx/dialog/templates/InputMessage.fxml"));
            Stage messageStage = new Stage();

            messageStage.initModality(Modality.WINDOW_MODAL);
            if (logo != null) {
                messageStage.getIcons().add(logo);
            }
            messageStage.initOwner(parent);
            messageStage.setTitle(title);
            messageStage.setResizable(false);
            messageLogoPath = "javafx/dialog/img/information.png";
            InputMessageController controller = new InputMessageController(message, messageLogoPath);
            loader.setController(controller);
            Pane indexViewPane = loader.load();
            Scene indexViewScene = new Scene(indexViewPane);
            messageStage.setScene(indexViewScene);
            messageStage.showAndWait();
            return controller.tInputText.getText();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }
    
    public static String showPasswordMessage(Window parent, String title, String message, Image logo) {
        try {
            FXMLLoader loader = new FXMLLoader(JXOptionPane.class.getResource("/javafx/dialog/templates/PasswordMessage.fxml"));
            Stage messageStage = new Stage();

            messageStage.initModality(Modality.WINDOW_MODAL);
            if (logo != null) {
                messageStage.getIcons().add(logo);
            }
            messageStage.initOwner(parent);
            messageStage.setTitle(title);
            messageStage.setResizable(false);
            messageLogoPath = "javafx/dialog/img/key.png";
            PasswordMessageController controller = new PasswordMessageController(message, messageLogoPath);
            loader.setController(controller);
            Pane indexViewPane = loader.load();
            Scene indexViewScene = new Scene(indexViewPane);
            messageStage.setScene(indexViewScene);
            messageStage.showAndWait();
            return controller.tInputText.getText();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }    

    public static void showInformationMessage(Window parent, String title, String message, Integer type, Image logo) {
        try {
            FXMLLoader loader = new FXMLLoader(JXOptionPane.class.getResource("/javafx/dialog/templates/InformationMessage.fxml"));
            Stage messageStage = new Stage();

            messageStage.initModality(Modality.WINDOW_MODAL);
            if (logo != null) {
                messageStage.getIcons().add(logo);
            }
            messageStage.initOwner(parent);
            messageStage.setTitle(title);
            messageStage.setResizable(false);

            switch (type) {
                case INFORMATION_MESSAGE:
                    messageLogoPath = "javafx/dialog/img/information.png";
                    break;
                case WARNING_MESSAGE:
                    messageLogoPath = "javafx/dialog/img/warning.png";
                    break;
                case ERROR_MESSAGE:
                    messageLogoPath = "javafx/dialog/img/error.png";
                    break;
            }

            InformationMessageController controller = new InformationMessageController(message, messageLogoPath);
            loader.setController(controller);

            Pane indexViewPane = loader.load();
            Scene indexViewScene = new Scene(indexViewPane);
            messageStage.setScene(indexViewScene);
            messageStage.showAndWait();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println(showPasswordMessage(null, "Prueba de entrada", "¿Desea cambiar este dato?", null));
    }

}
