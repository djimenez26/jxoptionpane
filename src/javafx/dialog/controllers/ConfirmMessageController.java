/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx.dialog.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Certificaciones
 */
public class ConfirmMessageController implements Initializable {

    private final String message;
    private final String imgPath;
    public Integer returnOption;
    
    @FXML
    public Label lMessage;
    @FXML
    public ImageView imgMessage;
    
    public ConfirmMessageController(String message, String imgPath) {
        this.message = message;
        this.imgPath = imgPath;
    }
    
    @FXML
    public void closeMessage(ActionEvent e) {
        returnOption = 0;
        ((Node)e.getSource()).getScene().getWindow().hide();
    }
    
    @FXML
    public void returnYesOption(ActionEvent e) {
        returnOption = JXOptionPane.YES_OPTION;
        ((Node)e.getSource()).getScene().getWindow().hide();        
    }
    
    @FXML
    public void returnNoOption(ActionEvent e) {
        returnOption = JXOptionPane.NO_OPTION;
        ((Node)e.getSource()).getScene().getWindow().hide();        
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lMessage.setText(message);
        imgMessage.setImage(new Image(imgPath));
    }    
    
}
